const cheerio = require('cheerio');
const request = require('request-promise');
const fs = require('fs');
const baseUrl = 'https://www.bankmega.com/';

(async function () {
    try {
        const $ = cheerio.load(await request(baseUrl + '/promolainnya.php'));
        let promises = $('div[id="subcatpromo"]').find('img').get().map((img, i) => scrapePerCategory({'title': $(img).attr('title'), 'category': i + 1}));
        const jobs = await Promise.all(promises);
        console.log(jobs);
    } catch (e) {
        console.log('error in main ', e);
    }
})();

const scrapePerCategory = async (job) => {
    try {
        let pageNumber;
        let page = 1;
        const results = [];
        do {
            let url = baseUrl + `/ajax.promolainnya.php?product=1&subcat=${job.category}&page=${page}`;
            let $ = cheerio.load(await request(url));
            if (!pageNumber) {
                pageNumber = $('a.page_promo_lain[id]').length;
            }
            let innerResults = [];
            let counterInstance = $('#promolain').find('a');
            for(var i=0; i<counterInstance.length; i++) { 
                let promoElement = cheerio(counterInstance[i]);
                let $ = cheerio.load(await request(baseUrl + promoElement.attr('href')));
                innerResults.push({
                    'title': promoElement.find('img').attr('title'), 
                    'imageurl': baseUrl + $('.keteranganinside').find('img').attr('src')
                });
            }
            results.push(innerResults); 
            page++;
        } while (page <= pageNumber);
        console.log(job.title + " Category scraped");
        let res = JSON.stringify(results, null, 2);
        fs.writeFile('results.json', res, function(err){
            if(err) {
                return console.log(err);
            }
            console.log("Done!");
        });
        return res;
    } catch (e) {
        console.log('error', e);
        throw e;
    }
};